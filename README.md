# README #

Traum library - one of the most famous electronic libraries in Russian Internet. Contains tens of thousands files in various formats. Unfortunately, it no longer updates, but I have its 2011 year version. It contains, among others, "ENG EPUB" volume (ISO file). Having size 7.27Gb it includes 12'897 epub files - books of different authors, from Aaronovitch Ben till Zubrin Robert.

Well, I have a couple of interesting ideas how to use this huge amount of data. But first I need to structurize it, check its validity, clean, insert into database and so on. That's why this project is created.

### Key Java elements used in it: ###
* Maven
* Log4j2
* Hibernate