import java.io.IOException;

public interface EBookFile {
    Long ReadFromDisk() throws IOException;
    Long SplitIntoParagraphs();
    void SaveToDatabase();
}
