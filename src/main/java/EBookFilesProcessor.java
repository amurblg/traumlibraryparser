import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class EBookFilesProcessor {
    String path;

    public EBookFilesProcessor(String path) {
        this.path = path;
    }

    public void StartProcessing() throws IOException {
        File file = new File(this.path);

        if (!file.isDirectory()) {
            EBookFile ef = new EPubFile(path);
            ef.ReadFromDisk();
            // TODO: 25.12.2016  
        } else {
            String[] allFiles = file.list((dir, name) -> name.toLowerCase().endsWith("epub"));
            if (allFiles == null || allFiles.length == 0) {
                System.out.println("Directory " + path + " does not contains any epub files. Program ends.");
                return;
            }

            ArrayList<String> filesList = new ArrayList<>(Arrays.asList(allFiles));
            for (String path :
                    filesList) {
                EBookFile ef = new EPubFile(path);
                ef.ReadFromDisk();
                // TODO: 25.12.2016
            }
        }
    }
}
