import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

public class Main {
    private static Properties properties = new Properties();

    public static String GetProperty(String propertyKey){
        return properties.getProperty(propertyKey);
    }

    public static void main(String[] args) throws IOException {
        //Command line arguments checking
        if (args.length < 1) {
            //1st param - path to epub file or directory
            System.out.println("The 1st params need to be the path to epub file or to the directory with such files. Program ends.");
            return;
        }

        File file = new File(args[0]);
        if (file.isDirectory()) {
            if (!file.exists()) {
                System.out.println("Directory " + args[0] + " does not exists. Program ends.");
                return;
            }
        } else {
            if (!file.exists()) {
                System.out.println("File " + args[0] + " does not exists. " +
                        "Program ends.");
                return;
            }
        }

        //.properties file init
        properties.load(new FileInputStream("tlp.properties"));
        Set<String> keys = properties.stringPropertyNames();
        for (String key :
                keys) {
            properties.put(key, properties.getProperty(key));
        }

        //finally - the book(-s) processing
        EBookFilesProcessor efp = new EBookFilesProcessor(args[0]);
        efp.StartProcessing();
    }
}
