import nl.siegmann.epublib.domain.Book;
import nl.siegmann.epublib.epub.EpubReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class EPubFile implements EBookFile {
    private String fullPath;
    private Book book;

    public EPubFile(String fullPath) {
        this.fullPath = fullPath;
    }

    @Override
    public Long ReadFromDisk() throws IOException {
        EpubReader er = new EpubReader();
        book = er.readEpub(new FileInputStream(fullPath));
        File f = new File(fullPath);
        return f.length();
    }

    @Override
    public Long SplitIntoParagraphs() {
        return null;
    }

    @Override
    public void SaveToDatabase() {

    }
}
